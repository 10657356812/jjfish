﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CombineMeshUV : EditorWindow
{
    [MenuItem("Tools/CombineMeshUV")]
    public static void CombineMeshByTwoUV()
    {
        GetWindow<CombineMeshUV>("Editor");
    }
    private Mesh meshOne;
    private Mesh meshTwo;
    //private Mesh meshThree;
    private GameObject obj;
    private Color[] colors = new Color[30]{ new Color(1,1,0,1), new Color(0,1,1,1), new Color(0,1,1,1), new Color(1, 1 ,0, 1), new Color(0,1,1,1),
    new Color(1, 0 ,0.5215687f, 1), new Color(1, 1 ,0 ,1), new Color(0,1,1,1), new Color(1, 0, 0.5215687f, 1), new Color(1, 1, 0, 1),
    new Color(0 ,1 ,1, 1), new Color(1 ,1 ,0, 1), new Color(1 ,0 ,0.9960784f, 1), new Color(1 ,0 ,0.9960784f, 1), new Color(1 ,0 ,0.9960784f, 1)
     , new Color(1 ,0 ,0.5215687f, 1), new Color(1, 0, 0.9960784f, 1), new Color( 1, 0, 0.5215687f, 1), new Color(1 ,0 ,0.5215687f, 1), new Color(1, 0, 0.9960784f, 1)
    , new Color(1 ,0, 0.9960784f, 1), new Color(1 ,0 ,1, 1), new Color(1 ,0 ,0.9960784f, 1), new Color(1, 0, 1, 1), new Color(1 ,0 ,0.9960784f, 1)
    , new Color(1 ,0, 1, 1), new Color(1 ,0 ,0.9960784f, 1), new Color(1, 0, 1, 1), new Color(1 ,0 ,0.9960784f, 1), new Color(1 ,0 ,1, 1)};
    private void OnGUI()
    {
        meshOne = EditorGUILayout.ObjectField("Mesh One", meshOne, typeof(Mesh), true) as Mesh;
        meshTwo = EditorGUILayout.ObjectField("Mesh Two", meshTwo, typeof(Mesh), true) as Mesh;
        //meshThree = EditorGUILayout.ObjectField("Mesh Three", meshThree, typeof(Mesh), true) as Mesh;
        obj = EditorGUILayout.ObjectField("obj", obj, typeof(GameObject), true) as GameObject;
        if(GUILayout.Button("CombineMesh"))
        {
            if(meshOne != null && meshTwo != null)
            {
                CombineInstance combineInstance = new CombineInstance();
                Vector2[] uvOne = meshOne.uv;
                Vector2[] uvTwo = meshTwo.uv;
                combineInstance.mesh = meshOne;
                combineInstance.mesh.uv = uvOne;
                combineInstance.mesh.uv2 = uvTwo;
                combineInstance.mesh.colors = colors;// meshThree.uv;
                combineInstance.mesh.vertices = meshOne.vertices;
                combineInstance.mesh.triangles = meshOne.triangles;
                obj.GetComponent<MeshFilter>().sharedMesh.CombineMeshes(new CombineInstance[]{ combineInstance}, false, false);
            }
        }
    }
}
