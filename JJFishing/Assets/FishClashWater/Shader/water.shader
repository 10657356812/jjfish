﻿Shader "FishingClash/water"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_WaveNormal("NormalWave", 2D) = "bump"{}
		_Sky("Sky", 2D) = "black"{}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
				float2 worldDire : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			sampler2D _WaveNormal;
			float4 _WaveNormal_ST;
			sampler2D _Sky;
			float4 _Sky_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

				float4 u_xlat0 = v.vertex.yyyy * unity_ObjectToWorld[1];
				u_xlat0 = unity_ObjectToWorld[0] * v.vertex.xxxx + u_xlat0;
				u_xlat0 = unity_ObjectToWorld[2] * v.vertex.zzzz + u_xlat0;
				u_xlat0 += unity_ObjectToWorld[3];
				//o.vertex = mul(UNITY_MATRIX_VP, u_xlat0);

                o.uv.xy = v.uv.xy;
                o.uv.zw = v.uv2.xy;
				//mul(unity_ObjectToWorld, v.vertex);

				float2 dir = v.vertex.yy * unity_ObjectToWorld[1].xz;
				dir = unity_ObjectToWorld[0].xz * v.vertex.xx + dir;
				dir = unity_ObjectToWorld[2].xz * v.vertex.zz + dir;
				dir = unity_ObjectToWorld[3].xz * v.vertex.ww + dir;


				float2 aa = float2(dot(unity_ObjectToWorld._11_12_13_14, v.vertex.xyzw), dot(unity_ObjectToWorld._31_32_33_34, v.vertex.xyzw));
				o.worldDire = mul(unity_ObjectToWorld, v.vertex).xz * 0.2;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
				float2 dir = i.worldDire.yx + 0.12f;
				float4 waveSp = _Time.y * float4(0.03, -0.02, 0.09, -0.05);
				dir = dir * 1.13 + waveSp.zw;
				float2 dir2 = i.worldDire.xy * float2(0.5, 1) - waveSp.xy;

				float2 waterNoise1 = tex2D(_WaveNormal, dir2).xy;
				float2 waterNoise2 = tex2D(_WaveNormal, dir).xy;
				
				float4 waterNoise = waterNoise1.xyxy + waterNoise2.xyxy;
				waterNoise = waterNoise - 1;
				float reflecArea = i.uv.w * i.uv.w;
				float u_xlat5 = waterNoise.z * reflecArea * 12 + 0.6;
				reflecArea = reflecArea * i.uv.w;
				reflecArea = reflecArea * u_xlat5;
				waterNoise = waterNoise * float4(0.1,0.1,0,1) + i.uv.zwxy;
				//uvZ = i.uv.w * i.uv.w * i.uv.w;
				//uvZ = clamp(uvZ, 0, 1) + 0.01;
				float noise = 0.9 - reflecArea;

				fixed4 reflecColor = tex2D(_Sky, waterNoise.zw);
				fixed4 water = tex2D(_MainTex, waterNoise.xy);
				//reflecColor = reflecColor * reflecArea * 1.3;
				fixed4 final = reflecColor * reflecArea * 1.3 + water* noise;
				final += reflecColor.w * 0.7;

				//final.rg = i.worldDire.xy;//i.uv.www;
				//final.b = 0;
                return final;//tex2D(_MainTex, i.uv.xy);;
            }
            ENDCG
        }
    }
}
