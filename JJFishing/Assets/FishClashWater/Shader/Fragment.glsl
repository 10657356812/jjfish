#version 100

#ifdef GL_FRAGMENT_PRECISION_HIGH
    precision highp float;
#else
    precision mediump float;
#endif
precision highp int;
uniform 	vec4 _Time;
uniform lowp sampler2D _t_waves_00_n;
uniform lowp sampler2D _bedrock;
uniform lowp sampler2D _water;
varying highp vec4 vs_TEXCOORD0;
varying highp vec4 vs_TEXCOORD1;
#define SV_Target0 gl_FragData[0]
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
lowp vec4 u_xlat10_0;
vec4 u_xlat1;
lowp vec4 u_xlat10_2;
vec4 u_xlat3;
float u_xlat5;
vec2 u_xlat8;
lowp vec2 u_xlat10_8;
void main()
{
    u_xlat0.xy = vs_TEXCOORD1.yx + vec2(0.119999997, 0.119999997);
    u_xlat1 = _Time.yyyy * vec4(0.0299999993, -0.0199999996, 0.0900000036, -0.0500000007);
    u_xlat0.xy = u_xlat0.xy * vec2(1.13, 1.13) + u_xlat1.zw;
    u_xlat8.xy = vs_TEXCOORD1.xy * vec2(0.5, 1.0) + (-u_xlat1.xy);
    u_xlat10_8.xy = texture2D(_t_waves_00_n, u_xlat8.xy).xy;
    u_xlat10_0.xy = texture2D(_t_waves_00_n, u_xlat0.xy).xy;
    u_xlat16_0 = u_xlat10_0.xyxy + u_xlat10_8.xyxy;
    u_xlat16_0 = u_xlat16_0 + vec4(-1.0, -1.0, -1.0, -1.0);
    u_xlat1.x = vs_TEXCOORD0.w * vs_TEXCOORD0.w;
    u_xlat5 = u_xlat16_0.z * u_xlat1.x;
    u_xlat0 = u_xlat16_0 * vec4(0.100000001, 0.100000001, 0.0, 1.0) + vs_TEXCOORD0.zwxy;
    u_xlat1.x = u_xlat1.x * vs_TEXCOORD0.w;
    u_xlat5 = u_xlat5 * 12.0 + 0.600000024;
    u_xlat1.x = u_xlat5 * u_xlat1.x;
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
    u_xlat1.x = u_xlat1.x + 0.00999999978;
    u_xlat5 = (-u_xlat1.x) + 0.899999976;
    u_xlat10_2 = texture2D(_water, u_xlat0.zw);
    u_xlat10_0 = texture2D(_bedrock, u_xlat0.xy);
    u_xlat3 = u_xlat1.xxxx * u_xlat10_2;
    u_xlat3 = u_xlat3 * vec4(1.29999995, 1.29999995, 1.29999995, 1.29999995);
    u_xlat0 = u_xlat10_0 * vec4(u_xlat5) + u_xlat3;
    u_xlat0 = u_xlat10_2.wwww * vec4(0.699999988, 0.699999988, 0.699999988, 0.699999988) + u_xlat0;
    SV_Target0 = u_xlat0;
    return;
}