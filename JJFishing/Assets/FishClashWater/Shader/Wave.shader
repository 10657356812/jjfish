﻿Shader "Unlit/Wave"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_node("Power",float) = 7.8
		_Mult("Mult",float) = 3.44
		_WaterFoam("WaterFoam", 2D) = "white"{}
		_WaterFoam2("WaterFoam2", 2D) = "white"{}
		_LightMap("_LightMap", 2D) = "white"{}
    }
    SubShader
    {
        Tags { "RenderType"="Transform" "Queue"="Transparent" }
        LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
				float4 color : COLOR;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD2;
                float4 vertex : SV_POSITION;
				float4 color : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			sampler2D _WaterFoam;
			float4 _WaterFoam_ST;
			sampler2D _WaterFoam2;
			float4 _WaterFoam2_ST;
			sampler2D _LightMap;
			float4 _LightMap_ST;
			float _node;
			float _Mult;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				float2 uvTem = sin((_Time.y + _node) * 0.5)  * float2(-0.100000001, 0.150000006) + v.uv;
				o.uv.xy = _Time.y * float2(-0.05,0.05) + uvTem;
				float2 uvTem2 = sin(_Time.y * 0.5)  * float2(-0.100000001, 0.150000006) + v.uv;
				o.uv.zw = _Time.y * float2(-0.05,0.05) + uvTem2;

				o.color = v.color;
				o.uv2 = v.uv2;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 foamUV2 = i.uv.xy * _WaterFoam2_ST.xy + _WaterFoam2_ST.zw;
				float waterFoam2Col  = tex2D(_WaterFoam2, foamUV2).x;
				float2 foamUV = i.uv.zw * _WaterFoam_ST.xy + _WaterFoam_ST.zw;
				float waterFoamCol  = tex2D(_WaterFoam, foamUV).x;
				float waterFoamTotal = waterFoamCol + waterFoam2Col;

				float3 main = tex2D(_MainTex, float2(i.uv2.xy * _MainTex_ST.xy + _MainTex_ST.zw)).xyz;
				float3 final = main + waterFoamTotal;

				float2 lightMapUV = i.uv2 * _LightMap_ST.xy + _LightMap_ST.zw;
				float3 lightCol = tex2D(_LightMap, lightMapUV).xyz;
				final = final * lightCol * _Mult;

				final = ((1.2 - i.uv2.y )* main + i.uv2.y * 1.3f) * final;

                fixed4 col = tex2D(_MainTex, i.uv.xy);
				col.a = i.color.y;
				col.rgb = final;
                return col;
            }
            ENDCG
        }
    }
}
