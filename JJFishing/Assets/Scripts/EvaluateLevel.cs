public enum EvaluateLevel
{
    Miss = 0,
    Bad = 1,
    Good = 2,
    Great = 3,
    Perfect = 4
}


public enum FishingState
{
    //Ready,//准备
    ThrowFishingRod,//抛鱼竿
    WaitForFish,//等待鱼上钩
    TakeBackFishingRod,//收杆
    FishWalking,//溜鱼
    End//结束
}