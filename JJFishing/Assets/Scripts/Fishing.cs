﻿using System;
using System.Collections;
using System.Collections.Generic;
using EZCameraShake;
using FairyGUI;
using UnityEngine;

/// <summary>
/// 溜鱼
/// </summary>
public class Fishing : MonoBehaviour
{
    public bool isdebug = true;
    
    #region 进度管理

    private FishingState m_state = FishingState.ThrowFishingRod;
    private bool isFirst = true;

    #endregion


    #region View

    public GComponent GCom; //溜鱼界面
    private Controller m_stage; //界面控制器
    private GTextField m_info; //提示信息

    //抛竿
    private GButton m_btnCasting; //抛竿按钮
    private GComponent m_castingCom; //抛竿组件
    private GProgressBar m_castingBar; //抛竿进度条,锚点在右下角
    private GComponent m_smallBar; //进度条中心组件
    private GObject m_target; //鱼的绿色区域，锚点在左中央
    private Controller m_castingBarColor; //抛竿条控制器

    //等待
    private GComponent m_winDragFish; //提前收杆的确认窗口
    private Transition m_tranWinDragFish; //提前收杆的确认窗口的动效
    private GButton m_btnAdvanceYes; //确定提前收杆
    private GButton m_btnAdvanceNo; //取消提前收杆
    private GObject m_maskAdvance; //弹出询问窗口时的遮罩


    //收杆
    private GButton m_btnDragRod; //收杆按钮

    //溜鱼
    private GButton m_btnCatch; //捕捉按钮
    private GComponent m_catchingBar; //捕捉条组件
    private Transition m_trans; //小鱼来回移动的动效
    private Controller m_stateCtrl; //切换小鱼动画的控制器
    private GProgressBar m_HP; //小鱼的血条
    private GTextField m_dam; //伤害数字
    private GObject m_flag; //旗帜下的那个区域点
    private GTextField m_txtEvaluate; //每次捕捉的评价

    //结算
    private GButton m_btnRestart; //再来一次按钮
    private GButton btnBack; //结束按钮

    #endregion


    #region Data

    //公有
    [Header("鱼")] public float FishPosValue = 100f; //抛竿阶段-抛竿条目标区域的位置
    public float FishWeight = 59f; //收杆阶段-鱼的重量
    public int FishMaxHP = 500; //溜鱼阶段-鱼的最大血量
    public float FishDifficultyVlaue = 100f; //溜鱼阶段-鱼的难度
    public float NormalSpeed = 1.0f; //溜鱼阶段-鱼正常游动时的速度
    public float HitSpeed = 2.0f; //溜鱼阶段-命中时，鱼加速游动的速度
    public float HitTime = 0.2f; //溜鱼阶段-命中时，鱼加速游动的时间（单位：秒）
    public float AccelerationTime = 0.8f; //溜鱼阶段-小鱼减速到正常速度所需的时间（从加速结束后开始计时，单位：秒）
    public float MissTime = 1.0f; //溜鱼未命中时，停留在原地播放动画的时间（单位：秒）

    [Header("鱼竿")] public float RulerMax = 160f; //抛竿阶段-抛竿条的长度
    public float CastingTargetWidth = 30f; //抛竿阶段-目标区域的宽度
    public float RodPower = 100f; //收杆阶段-鱼竿的承重
    public float FishingRodVlaue = 100f; //溜鱼阶段-鱼竿的易控属性

    [Header("计算")] public float BasicDamage = 10.0f; //溜鱼阶段-命中时的基础伤害
    public float[] DamageGrowthRate = new[] {0f, 0.85f, 1.0f, 1.3f}; //溜鱼阶段-溜鱼伤害增幅倍数（1+x）：bad|good|great|perfect

    [Header("界面")]
    //抛竿
    public float m_speed = 50f; //抛竿阶段-长按一秒蓄力多少

    public float PrefectRatio = 0.00f; //抛竿阶段-完美评价的误差范围
    public float GreatRatio = 0.1f; //抛竿阶段-很好评价的误差范围
    public float GoodRatio = 0.2f; //抛竿阶段-不错评价的误差范围

    //public float BadRatio = 0.3f;//抛竿阶段-糟糕
    //溜鱼
    public float GreenColorRatio = 40f; //溜鱼阶段-绿色块的初始占比（单位：%）
    [Range(0,5)]
    public int MinHitNum = 3; //溜鱼阶段-最少显示几个色块（取值[0,5]）
    [Range(0,5)]
    public int MaxHitNum = 5; //溜鱼阶段-最多显示几个色块(取值[0,5])
    //public float[] WidthMultiple = new[] {1.0f, 1.5f};//随机的命中区域的宽度区间
    public float WidthMultiple = 1.0f;//随机的命中区域的宽度倍数


    //私有
    //溜鱼
    private float m_unit = 1.0f;
    private float m_realBarHight = 0.0f; //抛竿条的最大长度
    private float[] m_targetRegion = new float[2]; //目标区间
    private float m_speedSign = 1; //标记进度条是增长还是减少

    //抛竿
    //可以命中的区域
    private float[] m_possibleRage = new[] {0.05f, 0.95f}; //两侧是掉头区域，色块只会出现在进度条中心90%区域的位置
    private List<GComponent> m_hits = new List<GComponent>();
    private List<float[]> m_hitRegion = new List<float[]>();
    private List<float> m_originWiths = new List<float>();
    private List<float> m_colorRatio = new List<float>(); //所有色块的占比-递增
    private float[] m_scoreInterval = new float[4];
    private float m_maxWidth = 0; //溜鱼条的长度
    private int m_realHitNum = 6; //所有色块的数量
    private int m_showHitNum = 0; //当次实际显示的色块的数量

    //状态
    private bool m_storaging = false; //是否正在蓄力-
    private bool isMoving = false; //在小鱼正在游动的时候，用户才能点击按钮
    private bool m_isChangeSpeed = false; //是否正在加速|减速

    //协程
    private Coroutine m_corFishSwitch = null; //鱼的动画切换
    private Coroutine m_corShowEvaluate = null; //评价的显示
    private Coroutine m_corShowDam = null; //伤害数字的显示
    [Header("调试用")] 
    public FishingState StartView = FishingState.ThrowFishingRod;
    [SerializeField] private float nowSpeed = 0.0f;
    [SerializeField] private float m_acceleration = 0.1f; //小鱼减速的速度

    #endregion


    private void Awake()
    {
        UIPackage.AddPackage("FGUI/Common");
        UIPackage.AddPackage("FGUI/Game");
        GCom = UIPackage.CreateObject("Game", "ViewFishing").asCom;
        GRoot.inst.AddChild(GCom);

        Init();
    }

    private void Update()
    {
        switch (m_state)
        {
            case FishingState.ThrowFishingRod:
                if (m_storaging)
                {
                    if (m_castingBar != null)
                    {
                        //计算值
                        double newValue = m_castingBar.value + m_speedSign * m_speed * Time.deltaTime;
                        if (newValue >= m_castingBar.max)
                        {
                            m_castingBar.value = m_castingBar.max;
                            m_speedSign = -1; //接下来该减少了
                        }
                        else if (newValue <= m_castingBar.min)
                        {
                            m_castingBar.value = m_castingBar.min;
                            m_speedSign = 1; //接下来该减增加了
                        }
                        else
                        {
                            m_castingBar.value = newValue;
                        }

                        if (m_castingBarColor != null)
                        {
                            SetBarColor();
                        }
                    }
                }

                break;
            case FishingState.FishWalking:
                if (m_trans != null)
                {
                    nowSpeed = m_trans.timeScale;
                }


                if (!m_isChangeSpeed || m_trans == null)
                {
                    return;
                }

                //减速
                m_acceleration = m_acceleration = (HitSpeed - NormalSpeed) / AccelerationTime;
                float newSpeed = m_trans.timeScale - m_acceleration * Time.deltaTime;
                if (newSpeed > NormalSpeed)
                {
                    m_trans.timeScale = newSpeed;
                }
                else
                {
                    m_trans.timeScale = NormalSpeed;
                    m_isChangeSpeed = false;
                    m_acceleration = 0.0f;
                }

                break;
        }
    }


    private void ChangeState(FishingState state)
    {
        switch (state)
        {
            /*case FishingState.Ready:
                break;*/
            case FishingState.ThrowFishingRod:
                m_stage.selectedIndex = 0;
                m_info.text = "长按蓄力";
                break;
            case FishingState.WaitForFish:
                m_stage.selectedIndex = 1;
                m_winDragFish.visible = false;
                m_maskAdvance.visible = false;
                m_btnDragRod.grayed = true;
                m_tranWinDragFish.Stop();
                m_info.text = "正在等鱼咬钩";

                DoItLater(5.0f, () => ChangeState(FishingState.TakeBackFishingRod));
                break;
            case FishingState.TakeBackFishingRod:
                m_stage.selectedIndex = 1;
                m_winDragFish.visible = false;
                m_maskAdvance.visible = false;
                m_btnDragRod.grayed = false;
                m_info.text = "钓到鱼啦！请收杆";
                break;
            case FishingState.FishWalking:
                RefreshHitRegion();
                m_stage.selectedIndex = 2;
                m_info.text = "正在溜鱼";
                //播放动画
                SetFishView(-1);
                break;
            case FishingState.End:
                m_info.text = "抓到鱼啦";
                m_stage.selectedIndex = 3;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }

        m_state = state;
    }


    //初始化
    private void Init()
    {
        m_stage = GCom.GetController("stage");
        m_info = GCom.GetChild("txtStatus").asTextField;


        #region 抛鱼竿

        m_btnCasting = GCom.GetChild("btnCasting").asButton;
        //m_btnCasting.onClick.Clear();
        m_btnCasting.touchable = true;
        m_btnCasting.onTouchBegin.Clear();
        m_btnCasting.onTouchEnd.Clear();
        m_btnCasting.onTouchBegin.Add(() =>
        {
            m_info.text = "蓄力中，松开抛竿";
            m_storaging = true;
        });
        m_btnCasting.onTouchEnd.Add(() =>
        {
            m_storaging = false;
            m_btnCasting.touchable = false;
            m_info.text = GetCastingEvaluateStr(GetCastingEvaluate());
            //等待一会儿后进入下一阶段
            DoItLater(3.0f, () =>
            {
                m_castingBar.value = m_castingBar.min;
                m_btnCasting.touchable = true;
                ChangeState(FishingState.WaitForFish);
            });
        });

        m_castingCom = GCom.GetChild("fishCasting").asCom;
        m_unit = m_castingCom.height / 100f; //单位长度
        m_unit = 2.0f; //todo:统一各个界面的度量衡
        //设置抛竿条的高度
        m_castingCom.height = m_unit * RulerMax;
        m_castingBar = m_castingCom.GetChild("castingBar").asProgress;
        m_smallBar = m_castingBar.GetChild("bar_v").asCom; //锚点是左上
        m_castingBar.max = RulerMax; //设置最大值
        //获得最大高度
        m_castingBar.value = m_castingBar.max;
        m_realBarHight = m_smallBar.height;
        //设置鱼的位置
        m_castingBar.value = FishPosValue;
        m_target = m_castingCom.GetChild("target"); //锚点是左中

        m_castingBarColor = m_smallBar.GetController("state");

        //设置目标区域的宽度
        m_target.height = CastingTargetWidth * m_unit;

        //初始化小鱼的位置
        if (FishPosValue <= RulerMax)
        {
            UpdateTargetPos(m_smallBar.position.y);
        }
        else //鱼的位置超过了抛竿条
        {
            //计算超过的比例
            double ratio = FishPosValue / RulerMax - 1;
            float offset = (float) (ratio * m_smallBar.height);
            UpdateTargetPos(m_smallBar.position.y - offset);
        }

        //设置初始高度
        m_castingBar.value = 0.0f;
        m_speedSign = 1;

        #endregion

        #region 等待

        m_winDragFish = GCom.GetChild("dragFishWindow").asCom;
        m_maskAdvance = GCom.GetChild("mask");
        m_tranWinDragFish = GCom.GetTransition("showWinAdvance");
        m_tranWinDragFish.Stop();
        GObject obj = m_winDragFish.GetChild("btnYes");
        m_btnAdvanceYes = m_winDragFish.GetChild("btnYes").asButton;
        m_btnAdvanceYes.onClick.Clear();
        m_btnAdvanceYes.onClick.Add(() =>
        {
            StopAllCoroutines();
            ChangeState(FishingState.ThrowFishingRod);
            Time.timeScale = 1.0f;
        });
        m_btnAdvanceNo = m_winDragFish.GetChild("btnNo").asButton;
        m_btnAdvanceNo.onClick.Clear();
        m_btnAdvanceNo.onClick.Add(() =>
        {
            m_winDragFish.visible = false;
            m_maskAdvance.visible = false;
            Time.timeScale = 1.0f; //todo:继续等待
        });

        #endregion

        #region 收杆

        m_btnDragRod = GCom.GetChild("btnDragRod").asButton;
        m_btnDragRod.onClick.Clear();
        m_btnDragRod.onClick.Add(DragRod);

        #endregion


        #region 溜鱼

        isMoving = false;
        m_btnCatch = GCom.GetChild("btnCatching").asButton;
        m_btnCatch.onTouchBegin.Clear();
        m_btnCatch.onTouchBegin.Add(CatchingFish);

        m_catchingBar = GCom.GetChild("catchingBar").asCom;
        m_HP = m_catchingBar.GetChild("hp").asProgress;
        m_HP.max = FishMaxHP;
        m_HP.value = m_HP.max;
        m_dam = m_catchingBar.GetChild("txtDamage").asTextField;
        m_txtEvaluate = m_catchingBar.GetChild("txtEvaluate").asTextField;

        m_flag = m_catchingBar.GetChild("flagSpot");
        m_maxWidth = m_catchingBar.GetChild("bg").width;
        m_stateCtrl = m_catchingBar.GetController("state"); //0游动|1命中|2未命中
        m_stateCtrl.selectedIndex = 0;
        m_trans = m_catchingBar.GetTransition("fishMove");

        //命中区域
        m_hits.Clear();
        m_originWiths.Clear();
        //记录所有的色块信息
        for (int i = 1; i <= m_realHitNum; i++)
        {
            var hit = m_catchingBar.GetChild($"hit{i}").asCom;
            //hit.pivot = new Vector2(0.5f,0.5f);
            //hit.pivotAsAnchor = true;

            m_hits.Add(hit);
        }
        
        InitHitView(FishingRodVlaue, FishDifficultyVlaue);
        
        RefreshHitRegion();

        #endregion

        #region 结算

        m_btnRestart = GCom.GetChild("btnRestart").asButton;
        m_btnRestart.onClick.Clear();
        m_btnRestart.onClick.Add(() =>
        {
            isFirst = false;
            StopAllCoroutines();
            Init();
        }); //todo:
        btnBack = GCom.GetChild("btnBack").asButton;
        btnBack.onClick.Add(Application.Quit); //todo:

        #endregion

        StartFishing(StartView);
    }


    //开始
    private void StartFishing(FishingState state)
    {
        ChangeState(state);
        //ChangeState(FishingState.FishWalking);
    }


    #region 抛竿

    private void SetBarColor()
    {
        //计算距离差值
        EvaluateLevel evaluate = GetCastingEvaluate();

        switch (evaluate)
        {
            case EvaluateLevel.Miss:
                break;
            case EvaluateLevel.Perfect:
                m_castingBarColor.selectedIndex = 3;
                break;
            case EvaluateLevel.Great:
                m_castingBarColor.selectedIndex = 2;
                break;
            case EvaluateLevel.Good:
                m_castingBarColor.selectedIndex = 1;
                break;
            case EvaluateLevel.Bad:
                m_castingBarColor.selectedIndex = 0;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    //0糟糕|
    private EvaluateLevel GetCastingEvaluate()
    {
        string evaluate = string.Empty;

        //计算距离差值
        float barPos = m_smallBar.position.y;
        float offset = 0.0f;

        //在区域上方
        if (barPos < m_targetRegion[0])
        {
            offset = Math.Abs(barPos - m_targetRegion[0]);
        }
        //在区域下方
        else if (barPos > m_targetRegion[1])
        {
            offset = Math.Abs(barPos - m_targetRegion[1]);
        }
        else //在区域中间
        {
            return EvaluateLevel.Perfect;
        }

        float end = offset / m_realBarHight;
/*        double offset = Math.Abs(m_castingBar.value - FishPosValue);
        double end = offset / m_castingBar.max;*/

        if (end >= 0 && end < PrefectRatio)
        {
            return EvaluateLevel.Perfect;
        }
        else if (end >= PrefectRatio && end < GreatRatio)
        {
            return EvaluateLevel.Great;
        }
        else if (end >= GreatRatio && end < GoodRatio)
        {
            return EvaluateLevel.Good;
        }
/*        else if(end >= 0.2f && end < 0.3f)
        {

        }*/
        else
        {
            return EvaluateLevel.Bad;
        }
    }


    private string GetCastingEvaluateStr(EvaluateLevel evaluate)
    {
        string str = string.Empty;
        switch (evaluate)
        {
            case EvaluateLevel.Bad:
            case EvaluateLevel.Miss:
                str = "[color=#FF0000]糟糕！[/color]";
                CameraShaker.Instance.ShakeOnce(4.14f, 2.05f, 0.97f, 1.0f, new Vector3(0.53f, 0.28f, 0f), Vector3.one);
                break;
            case EvaluateLevel.Good:
                str = "[color=#FFA500]不错[/color]";
                CameraShaker.Instance.ShakeOnce(1.0f, 1.0f, 1.0f, 1.0f, new Vector3(0.0f, 0.0f, 0.49f), Vector3.one);
                break;
            case EvaluateLevel.Great:
                str = "[color=#FFFF00]很好[/color]";
                CameraShaker.Instance.ShakeOnce(1.0f, 1.0f, 1.0f, 1.0f, new Vector3(0.0f, 0.0f, 0.49f), Vector3.one);
                break;
            case EvaluateLevel.Perfect:
                str = "[color=#008000]完美！[/color]";
                CameraShaker.Instance.ShakeOnce(2.92f, 0.3f, 2.02f, 0.26f, new Vector3(0.0f, 0.0f, 0.62f), Vector3.one);
                break;
        }

        return str;
    }


    private void UpdateTargetPos(float y)
    {
        var pos = m_target.position;
        m_target.SetPosition(pos.x, y, pos.z);
        m_targetRegion[0] = m_target.position.y - 0.5f * m_target.height; //上边界
        m_targetRegion[1] = m_target.position.y + 0.5f * m_target.height; //下边界
    }

    #endregion


    #region 收杆

    private void DragRod()
    {
        //是否是提前收杆
        if (m_state == FishingState.WaitForFish)
        {
            //弹出窗口进行确认
            m_winDragFish.visible = true;
            m_maskAdvance.visible = true;
            m_tranWinDragFish.Play();
            Time.timeScale = 0.0f; //todo:暂停
        }
        else
        {
            if (FishWeight <= RodPower) //承重足够
            {
                ChangeState(FishingState.FishWalking);
            }
            else
            {
                ChangeState(FishingState.End);
                m_info.text = "鱼挣脱了鱼竿！";
            }
        }
    }

    #endregion


    #region 溜鱼

    //刷新捕捉区域
    private void RefreshHitRegion()
    {
        foreach (var hit in m_hits)
        {
            hit.visible = false;
        }

        //随机显示数量
        if (MinHitNum >= MaxHitNum)
        {
            m_showHitNum = MaxHitNum;
        }
        else
        {
            m_showHitNum = RandomHelper.GetNormalRandom(MinHitNum, MaxHitNum + 1);
        }
        //Debug.LogError(string.Format("要展示{0}个色块", m_showHitNum));
        int tempNum = m_showHitNum + 1; //多出来的一个最后要取消
        if (tempNum > m_realHitNum) tempNum = m_realHitNum;

        //随机宽度0.7-2倍
        /*if (WidthMultiple[0] < WidthMultiple[1])
        {
            //float widthsum = 0f;//总宽度
            for (int i = 0; i < m_hits.Count; i++)
            {
                var hit = m_hits[i];
                int a = (int) (10 * WidthMultiple[0]);
                int b = (int) (10 * WidthMultiple[1]);
                int sa = RandomHelper.GetGuidRandom(a, b);
                //Debug.LogError(string.Format("{0}和{1}生成的随机数是{2}",a,b,sa));
                float sac = sa*0.1f;
                //Debug.LogError("随机数是"+sac);
                hit.width = sac * m_originWiths[i];
                //widthsum += hit.width;
            }
            //float
        }*/
        
        //更改宽度
        for (int i = 0; i < m_hits.Count; i++)
        {
            var hit = m_hits[i];
            hit.width = WidthMultiple * m_originWiths[i];
        }

        //位置随机分布
        //计算总宽度
        float widthsum = 0f; //总宽度
        for (int i = 0; i < tempNum; i++)
        {
            var hit = m_hits[i];
            widthsum += hit.width;
        }

        //计算总共有多少空白区域
        if (m_possibleRage[1] <= m_possibleRage[0])
        {
            Debug.LogError("参数错误！");
            return;
        }
        
        float blankWidth = (m_possibleRage[1] - m_possibleRage[0]) * m_maxWidth - widthsum;
        if (blankWidth < 0)
        {
            blankWidth = 0;
            Debug.LogError(string.Format("可用条{0}-色块总宽{1}",(m_possibleRage[1] - m_possibleRage[0]) * m_maxWidth, widthsum));
        }
        
        float itemBlank = blankWidth / (tempNum - 1);
        for (int i = 0; i < tempNum; i++)
        {
            var hit = m_hits[i];
            Vector3 pos = hit.position; //锚点是左上角
            if (i == 0)
            {
                hit.position = new Vector3(m_possibleRage[0] * m_maxWidth, pos.y, pos.z);
            }
            else
            {
                var lastHit = m_hits[i - 1];
                hit.position = new Vector3(lastHit.position.x + lastHit.width + itemBlank, pos.y, pos.z);
            }

            hit.visible = true;
        }

        //距离小鱼最近的那个不显示
        float minDis = float.MaxValue;
        int minIndex = 0;
        for (int i = 0; i < tempNum; i++)
        {
            float nowDis = Mathf.Abs(m_flag.position.x - m_hits[i].position.x);
            if (nowDis < minDis)
            {
                minDis = nowDis;
                minIndex = i;
            }
        }

        m_hits[minIndex].visible = false;

        //每个都朝小鱼的方向移动0.4-0.7个blank距离
        int fuhao = (m_flag.position.x - m_hits[minIndex].position.x) > 0 ? 1 : -1;
        //记录位置
        m_hitRegion.Clear();
        for (int i = 0; i < tempNum; i++)
        {
            var hit = m_hits[i].asCom;
            if (hit.visible == true)
            {
                //改变位置
                float ra = RandomHelper.GetNormalRandom(4, 7) * fuhao * 0.1f; //每个都朝小鱼的方向移动0.4-0.7个blank距离
                Vector3 pos = hit.position;
                hit.SetPosition(pos.x + ra * itemBlank, pos.y, pos.z);
                float left = hit.position.x;
                float right = hit.position.x + hit.width;
                float[] region = new float[2] {left, right};
                m_hitRegion.Add(region);
            }
        }

        InitHitView(FishingRodVlaue, FishDifficultyVlaue);

    }


    private void DoItLater(float delayedTime, Action action)
    {
        if (m_corFishSwitch != null)
        {
            StopCoroutine(m_corFishSwitch);
        }

        m_corFishSwitch = StartCoroutine(StartJobDelayed(delayedTime, action));
    }

    private IEnumerator StartJobDelayed(float delayedTime, Action action)
    {
        yield return new WaitForSeconds(delayedTime);
        action?.Invoke();
    }


    private void ShowEvaluate(float time, string text)
    {
        if (m_corShowEvaluate != null)
        {
            StopCoroutine(m_corShowEvaluate);
        }

        m_corShowEvaluate = StartCoroutine(IEShowEvaluate(time, text));
    }


    private IEnumerator IEShowEvaluate(float time, string text)
    {
        m_txtEvaluate.text = text;
        //移动位置
        m_txtEvaluate.SetPosition(m_flag.position.x, m_txtEvaluate.position.y, m_txtEvaluate.z);
        yield return new WaitForSeconds(time);
        m_txtEvaluate.text = "";
    }


    private void ShowDam(float time, string text)
    {
        if (m_corShowDam != null)
        {
            StopCoroutine(m_corShowDam);
        }

        m_corShowDam = StartCoroutine(IEShowDam(time, text));
    }


    private IEnumerator IEShowDam(float time, string text)
    {
        m_dam.text = text;
        yield return new WaitForSeconds(time);
        m_dam.text = "";
    }


    /// <summary>
    /// 设置小鱼的状态
    /// </summary>
    /// <param name="index">0游动|1命中|2未命中</param>
    private void SetFishView(int index, string info = "")
    {
        switch (index)
        {
            case -2: //抓捕成功
                m_stateCtrl.selectedIndex = 1; //切换动画
                m_trans.timeScale = NormalSpeed;
                m_trans.SetPaused(true); //停止播放动效
                m_trans.SetAutoPlay(false, -1, 0);
                isMoving = false; //设置参数
                m_info.text = !string.IsNullOrEmpty(info) ? info : "抓捕成功！";
                break;
            case -1: //初始化
                m_stateCtrl.selectedIndex = 0; //切换动画
                m_trans.timeScale = NormalSpeed;
                m_trans.Play(); //继续播放动效
                m_trans.SetAutoPlay(true, -1, 0);
                isMoving = true; //设置参数
                m_info.text = !string.IsNullOrEmpty(info) ? info : "游戏开始！";
                m_trans.SetHook("TrunBack", () =>
                {
                    RefreshHitRegion();
                    //Debug.LogError("转身");
                });
                m_trans.SetHook("Trun", () =>
                {
                    RefreshHitRegion();
                    //Debug.LogError("转");
                });
                break;
            case 0: //重新正常游
                m_stateCtrl.selectedIndex = 0; //切换动画
                //m_trans.timeScale = NormalSpeed;
                m_trans.SetPaused(false); //继续播放动效,
                isMoving = true; //设置参数
                m_isChangeSpeed = true; //开始减速
                //m_info.text =  !string.IsNullOrEmpty(info)? info: "进行中";
                break;
            case 1: //命中
                m_stateCtrl.selectedIndex = 1; //切换动画
                m_trans.timeScale = HitSpeed; //加速动效一段时间
                isMoving = true; //设置参数
                m_isChangeSpeed = false; //开始减速
                //m_info.text =  !string.IsNullOrEmpty(info)? info: "命中了！";
                break;
            case 2: //未命中
                m_trans.SetPaused(true); //暂停播放动效
                m_trans.timeScale = NormalSpeed;
                m_stateCtrl.selectedIndex = 2; //切换动画
                isMoving = false; //设置参数
                m_isChangeSpeed = false; //开始减速
                //m_info.text =  !string.IsNullOrEmpty(info)? info: "没抓到！";
                break;
        }
    }


    private void CatchingFish()
    {
        if (!isMoving)
        {
            m_info.text = "现在不能抓！";
            return;
        }


        var hitInfo = GetHitInfo();
        GetEvaluateStr(hitInfo);
        if (hitInfo != EvaluateLevel.Miss) //命中的情况
        {
            int damage = (int) (BasicDamage * (1 + GetHitScoreBuff(hitInfo))); //todo:勉强拉杆衰减和连击增伤
            int nextHP = (int) (m_HP.value - damage);
            if (nextHP < 0)
            {
                nextHP = 0;
            }

            if (nextHP > 0) //鱼还活着
            {
                SetFishView(1);
                m_HP.value = nextHP;
                DoItLater(HitTime, () => SetFishView(0));
            }
            else //抓到了，结束
            {
                SetFishView(-2);
                m_HP.value = nextHP;
                //切换状态
                DoItLater(3.0f, () =>
                {
                    m_trans.Stop();
                    ChangeState(FishingState.End);
                });
            }

            ShowDam(1.0f, (-damage).ToString());
        }
        else //未命中的情况
        {
            /*if(m_corShowDam!=null)
                StopCoroutine(m_corShowDam);
            m_dam.text = string.Empty;*/
            SetFishView(2);
            DoItLater(MissTime, () => SetFishView(0));
        }
    }


    //判断命中了没有:
    private EvaluateLevel GetHitInfo()
    {
        float pos = m_flag.position.x;
        float posLeft = (float) (m_flag.position.x - m_flag.width * 0.5);
        float posRight = (float) (m_flag.position.x + m_flag.width * 0.5);

        //foreach (var region in m_hitRegion)
        for (int i = 0; i < m_hitRegion.Count; i++)
        {
            var region = m_hitRegion[i];

            //命中了某一个
            //if ((pos >= region[0]) && (pos <= region[1]))
            if ((posRight >= region[0]) && (posLeft <= region[1]))
            {
                //真实命中
                if ((pos >= region[0]) && (pos <= region[1]))
                {
                    float center = (region[1] + region[0]) * 0.5f; //色块中心
                    float width = region[1] - region[0]; //色块宽度
                    float diff = Mathf.Abs(pos - center); //距离
                    float roat = diff / width; //距离占比

                    if (roat <= m_scoreInterval[0])
                    {
                        return EvaluateLevel.Perfect;
                    }
                    else if (roat > m_scoreInterval[0] && roat < m_scoreInterval[1])
                    {
                        return EvaluateLevel.Great;
                    }
                    else if (roat > m_scoreInterval[1] && roat < m_scoreInterval[2])
                    {
                        return EvaluateLevel.Good;
                    }
                    else
                    {
                        return EvaluateLevel.Bad;
                    }
                }
                else //模糊命中
                {
                    return EvaluateLevel.Bad;
                }
            }
        }

        //未命中
        return EvaluateLevel.Miss;
    }


    /// <summary>
    /// 初始化色块的外观
    /// </summary>
    /// <param name="fishingRodVlaue">鱼竿的易控度属性</param>
    /// <param name="fishDifficultyVlaue">鱼的难度属性</param>
    private void InitHitView(float fishingRodVlaue, float fishDifficultyVlaue)
    {
        //计算外观的数值
        float buff = (fishingRodVlaue / fishDifficultyVlaue);
        float greenRatio = buff * GreenColorRatio;
        float orangeRatio = Mathf.Floor((100 - greenRatio) / 3f);
        float redRatio = Mathf.Floor((100 - greenRatio) / 3f);
        float yellowRatio = 100 - greenRatio - orangeRatio - redRatio;

        float realGreenRatio = greenRatio / 100;
        float realYellowRatio = (greenRatio + yellowRatio) / 100;
        float realOrangeRatio = (greenRatio + yellowRatio + orangeRatio) / 100;
        float realRedRatio = 1.0f;

        m_colorRatio.Clear();
        m_colorRatio.Add(realGreenRatio);
        m_colorRatio.Add(realYellowRatio);
        m_colorRatio.Add(realOrangeRatio);
        m_colorRatio.Add(realRedRatio);

        m_scoreInterval[0] = realGreenRatio * 0.5f;
        m_scoreInterval[1] = realYellowRatio * 0.5f;
        m_scoreInterval[2] = realOrangeRatio * 0.5f;


        //更改外观:绿|黄|橙|红
        for (int i = 0; i < m_realHitNum; i++)
        {
            var hit = m_hits[i].asCom;
            //hit.pivot = new Vector2(0.5f,0.5f);
            //hit.pivotAsAnchor = true;
            
            m_originWiths.Add(hit.width);
            var red = hit.GetChild("red");
            red.width = hit.width * realRedRatio;
            var green = hit.GetChild("green");
            green.width = hit.width * realGreenRatio;
            var yellow = hit.GetChild("yellow");
            yellow.width = hit.width * realYellowRatio;
            var orange = hit.GetChild("orange");
            orange.width = hit.width * realOrangeRatio;

            //SetBlockPos(hit);

            //hit.visible = false;//一开始全隐藏
        }
    }


    private void SetBlockPos(GComponent hit)
    {
        Vector3 pos = Vector3.zero;
        float centerX = hit.position.x + hit.width * 0.5f;

        var red = hit.GetChild("red");
        pos = red.position;
        red.SetPosition(centerX, pos.y, pos.z);
        //red.position = centerPos;

        var green = hit.GetChild("green");
        pos = green.position;
        green.SetPosition(centerX, pos.y, pos.z);

        var yellow = hit.GetChild("yellow");
        pos = yellow.position;
        yellow.SetPosition(centerX, pos.y, pos.z);

        var orange = hit.GetChild("orange");
        pos = orange.position;
        orange.SetPosition(centerX, pos.y, pos.z);
    }

    //若命中，获得分数
    private float GetHitScoreBuff(EvaluateLevel hitInfo)
    {
        switch (hitInfo)
        {
            case EvaluateLevel.Miss:
            case EvaluateLevel.Bad:
                return DamageGrowthRate[0];
            case EvaluateLevel.Good:
                return DamageGrowthRate[1];
            case EvaluateLevel.Great:
                return DamageGrowthRate[2];
            case EvaluateLevel.Perfect:
                return DamageGrowthRate[3];
            default:
                return 0.0f;
        }
    }

    //获得评价
    private string GetEvaluateStr(EvaluateLevel hitInfo)
    {
        string evaluate = "";

        switch (hitInfo)
        {
            case EvaluateLevel.Miss:
                evaluate = "[color=#FF0000]失误[/color]";
                break;
            case EvaluateLevel.Bad:
                evaluate = "[color=#FF0000]糟糕！[/color]";
                break;
            case EvaluateLevel.Good:
                evaluate = "[color=#FFA500]不错[/color]";
                break;
            case EvaluateLevel.Great:
                evaluate = "[color=#FFFF00]很好[/color]";
                break;
            case EvaluateLevel.Perfect:
                evaluate = "[color=#008000]完美！[/color]";
                break;
            default:
                evaluate = "";
                break;
        }

        if (!string.IsNullOrEmpty(evaluate))
        {
            ShowEvaluate(1.0f, evaluate);
        }

        return evaluate;
    }

    #endregion
}