using System;

/// <summary>
    /// 生成随机数-工具类
    /// </summary>
    public class RandomHelper
    {
        /// <summary>
        /// 通过random.next 获取随机数
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>获取随机数</returns>
        public static int GetNormalRandom(int min,int max)
        {
            if (min >= max || min < 0)
                return min;
            int randomParam = new Random().Next(min,max);
            return randomParam;
/*            int iRandomBack = -1; //防溢出，固定回传
            if (min > max) //B大于A  
            {
                byte[] bytes = new byte[4];
                System.Security.Cryptography.RNGCryptoServiceProvider rng = new System.Security.Cryptography.RNGCryptoServiceProvider();  //采用 RNGCryptoServiceProvider取随机数
                rng.GetBytes(bytes);
                int Randseed = BitConverter.ToInt32(bytes, 0);  //产生一个随机种子
                
                string strTick = Convert.ToString(DateTime.Now.Ticks);  //采用DateTime.Now.Ticks取随机数
                if (strTick.Length > 8)
                    strTick = strTick.Substring(strTick.Length - 8, 8);  //绝大部分情况，tick后面的8位数字才是变动的，前面不变舍去
                Randseed = Randseed + Convert.ToInt32(strTick);  //第二个随机种子，加上第一个随机种子
                Random random = new Random(Randseed);
                iRandomBack = random.Next(min,max);
            }
            return iRandomBack;  //2020-08-25 经测试，快速调用，依然随机性好*/
        }

        /// <summary>
        /// 通过DateTime.Now.Ticks 获取随机数
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>获取随机数</returns>
        public static int GetDateRandom(int min,int max)
        {
            if (min >= max || min < 0  )
                return min;
            int gapValue = max - min;
            int randomParam=(int)(DateTime.Now.Ticks % gapValue);
            return randomParam+min;
        }

        /// <summary>
        /// 通过Guid 生成随机数
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>获取随机数</returns>
        public static int GetGuidRandom(int min ,int max)
        {
            if (min >= max || min < 0)
                return min;
            int gapValue = max - min;
            int randomParam = (int)(Guid.NewGuid().GetHashCode() % gapValue);
            return randomParam + min;
        }

        /// <summary>
        /// 通过RNGCryptoServiceProvider 生成随机数
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>获取随机数</returns>
        public static int GetRNGCryptoRandom(int min, int max)
        {
            if (min >= max || min < 0)
                return min;
            int gapValue = max - min;

            System.Security.Cryptography.RNGCryptoServiceProvider rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            byte[] bb = new byte[4];
            rng.GetBytes(bb);
            int randomParam = BitConverter.ToInt32(bb, 0) % gapValue;
            return randomParam + min;
        }
    }
